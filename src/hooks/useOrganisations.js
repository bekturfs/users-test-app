import ORGANISATIONS from "../data/organisations.json"

let organisations = []

if (ORGANISATIONS && ORGANISATIONS.organisations){
    organisations = ORGANISATIONS.organisations
}

function UseOrganisations() {
    return { organisations }
}

export default UseOrganisations