export const MODAL_TYPES = {
    NONE: 'NONE',
    EDIT: 'EDIT',
    CREATE: 'CREATE',
    DELETE: 'DELETE',
    DELETE_ALL: 'DELETE_ALL',
}