import React from 'react'
import {Modal, ModalBody, ModalFooter, ModalHeader} from "../modal"
import Button from "../ui/button"
import {useDispatch} from "react-redux"
import {deleteUser, deleteUsers} from "../../redux/actions/users"
import {MODAL_TYPES} from "../../constants"

function DeleteUsers({ modalState, onCloseModal }) {

    const dispatch = useDispatch()

    const { isOpen, type, user, users } = modalState

    const isDeleteAll = type === MODAL_TYPES.DELETE_ALL

    const onDelete = () => {

        if (isDeleteAll) {
            dispatch(deleteUsers(users.map(u => u.id)))
        } else if (type === MODAL_TYPES.DELETE) {
            dispatch(deleteUser(user.id))
        }

        onCloseModal()
    }

    if (!isOpen || (type !== MODAL_TYPES.DELETE && type !== MODAL_TYPES.DELETE_ALL )) {
        return null
    }

    return (
        <Modal open={isOpen} onClose={onCloseModal} size="md">
            <ModalHeader>
                <h3>Удаление { isDeleteAll ? 'пользователей' : 'пользователя' }</h3>
            </ModalHeader>
            <ModalBody>
                {
                    isDeleteAll ?
                        `Вы уверены, что хотите удалить пользователя ${user?.lastName} ${user?.firstName} ?` :
                        'Вы уверены, что хотите удалить этих пользователей'
                }
            </ModalBody>
            <ModalFooter>
                <Button color="danger" className="mr-10" onClick={onDelete}>
                    Удалить
                </Button>
                <Button color="secondary" onClick={onCloseModal}>
                    Отменить
                </Button>
            </ModalFooter>
        </Modal>
    )
}

export default DeleteUsers