import React, {useState} from 'react'
import UserRow from "./user-row"
import UsersTable from "./users-table"
import {useSelector} from "react-redux"
import {getPageNumbers, getUsersWithPagination} from "../../redux/getters/users"
import Pagination from "../pagination"
import Button from "../ui/button"
import CreateEditUser from "./create-edit-user"
import DeleteUsers from "./delete-users"
import {MODAL_TYPES} from "../../constants"
import useOrganisations from "../../hooks/useOrganisations"

const ITEMS_PER_PAGE = 10

function Users(props) {

    const [activePage, setActivePage] = useState(1)
    const [modalState, setModalState] = useState({ isOpen: false, user: null, users: [], type: MODAL_TYPES.NONE })
    const [selectedUsers, setSelectedUsers] = useState([])

    const users = useSelector(getUsersWithPagination(activePage, ITEMS_PER_PAGE))
    const pageNumbers = useSelector(getPageNumbers(ITEMS_PER_PAGE))
    const { organisations } = useOrganisations()

    const onCloseModal = () => setModalState({
        isOpen: false,
        user: null,
        users: [],
        type: MODAL_TYPES.NONE
    })

    const onCreateClick = () => setModalState({
        isOpen: true,
        user: null,
        users: [],
        type: MODAL_TYPES.CREATE
    })

    const onEditClick = (user) => setModalState({
        isOpen: true,
        user,
        users: [],
        type: MODAL_TYPES.EDIT
    })

    const onDeleteClick = (user) => setModalState({
        isOpen: true,
        user,
        users: [],
        type: MODAL_TYPES.DELETE
    })

    const toggleSelectUser = user => {
        if (selectedUsers.find(u => u.id === user.id)){
            setSelectedUsers(selectedUsers.filter(u => u.id !== user.id))
        } else {
            setSelectedUsers([ ...selectedUsers, user ])
        }
    }

    const onDeleteAllClick = () => setModalState({
        isOpen: true,
        user: null,
        users: selectedUsers,
        type: MODAL_TYPES.DELETE_ALL
    })

    return (
        <div className="users">
            <div className="container">

                <Button color="primary" className="users__add-btn" onClick={onCreateClick}>
                    Добавить пользователя
                </Button>

                <UsersTable>
                    { users.map(user =>
                        <UserRow
                            key={user.id}
                            user={user}
                            onEditClick={onEditClick}
                            onDeleteClick={onDeleteClick}
                            isSelected={selectedUsers.some(u => u.id === user.id)}
                            toggleSelectUser={toggleSelectUser}
                            organisations={organisations}
                        />)
                    }
                </UsersTable>

                <div className="users__footer">
                    <div>
                        { selectedUsers.length > 0 &&
                            <Button color="danger" className="mt-10" onClick={onDeleteAllClick}>
                                Удалить выбранные
                            </Button>
                        }
                    </div>
                    <Pagination pageNumbers={pageNumbers} activePage={activePage} setActivePage={setActivePage}/>
                </div>

                {
                    modalState.isOpen && <>
                        <CreateEditUser modalState={modalState} onCloseModal={onCloseModal}/>
                        <DeleteUsers modalState={modalState} onCloseModal={onCloseModal} selectedUsers={selectedUsers}/>
                    </>
                }

            </div>
        </div>
    )
}

export default Users