import React from 'react'

function UsersTable({ children }) {
    return (
        <table className="users__table">
            <thead className="users__table_head">
                <tr className="users__table_row">
                    <th className="users__table_col"></th>
                    <th className="users__table_col">ФИО</th>
                    <th className="users__table_col">Организация</th>
                    <th className="users__table_col">E-mail</th>
                    <th className="users__table_col">Удаление/изменение</th>
                </tr>
            </thead>
            <tbody className="users__table_body">
                { children }
            </tbody>
        </table>
    )
}

export default UsersTable