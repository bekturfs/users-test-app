import React, {useEffect, useState} from 'react'
import {Modal, ModalBody, ModalFooter, ModalHeader} from "../modal"
import UserForm from "./user-form"
import {useDispatch, useSelector} from "react-redux"
import {addUser, updateUser} from "../../redux/actions/users"
import {getMaxId} from "../../redux/getters/users"
import Button from "../ui/button"
import {MODAL_TYPES} from "../../constants"

function CreateEditUser({ modalState, onCloseModal }) {

    const dispatch = useDispatch()
    const maxId = useSelector(getMaxId)

    const [inputs, setInputs] = useState({})
    const [errors, setErrors] = useState({})

    const onSubmit = e => {
        e.preventDefault()

        const { isValid, errors } = validateUser(inputs, modalState.type === MODAL_TYPES.EDIT)

        if (!isValid) {
            setErrors(errors)
            return
        }

        const newUser = {
            firstName: inputs.firstName,
            lastName: inputs.lastName,
            middleName: inputs.middleName || '',
            organisationId: +inputs.organisationId || null,
            email: inputs.email,
        }

        if (modalState.type === MODAL_TYPES.EDIT) {
            dispatch(updateUser({ id: modalState.user.id, ...newUser }))
        } else if (modalState.type === MODAL_TYPES.CREATE) {
            maxId && dispatch(addUser({ id: maxId + 1, ...newUser }))
        }

        onCloseModal()
    }

    useEffect(() => {
        if (modalState && modalState.user) {
            setInputs({ ...modalState.user })
        }
    }, [modalState])

    if (!modalState.isOpen || (modalState.type !== MODAL_TYPES.CREATE && modalState.type !== MODAL_TYPES.EDIT )) {
        return null
    }

    return (
        <Modal open={modalState.isOpen} onClose={onCloseModal} size="md">
            <ModalHeader>
                <h3>{ modalState.type === MODAL_TYPES.EDIT ? 'Изменение пользователя' : 'Создание пользователя' }</h3>
            </ModalHeader>
            <ModalBody>
                <UserForm
                    inputs={inputs}
                    setInputs={setInputs}
                    errors={errors}
                    setErrors={setErrors}
                    onSubmit={onSubmit}
                    isEdit={modalState.type === MODAL_TYPES.EDIT}
                />
            </ModalBody>
            <ModalFooter>
                <Button color="primary" className="mr-10" onClick={onSubmit}>
                    Сохранить
                </Button>
                <Button color="secondary" onClick={onCloseModal}>
                    Отмена
                </Button>
            </ModalFooter>
        </Modal>
    )
}

const validateUser = (inputs, isEdit) => {

    const errors = {}
    const { firstName, lastName, organisationId, email } = inputs

    if (!firstName || !lastName || (!organisationId && isEdit) || !email) {
        !firstName && (errors.firstName = 'Обязательное поле')
        !lastName && (errors.lastName = 'Обязательное поле')
        !organisationId && isEdit && (errors.organizationId = 'Обязательное поле')
        !email && (errors.email = 'Обязательное поле')
        return { isValid: false, errors }
    }

    if (!validateEmail(email)) {
        errors.email = 'Некорректная электронная почта'
        return { isValid: false, errors }
    }

    return { isValid: true, errors }
}

const validateEmail = (email) =>
    email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)

export default CreateEditUser