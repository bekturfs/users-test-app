import React from 'react'
import useOrganisations from "../../hooks/useOrganisations"

function UserForm({ inputs, setInputs, errors, setErrors, onSubmit, isEdit }) {

    const { organisations } = useOrganisations()

    const handleChange = e => {
        setInputs(old => ({ ...old, [e.target.name]: e.target.value }))
        setErrors(old => ({ ...old, [e.target.name]: null }))
    }

    return (
        <div className="form__group" onSubmit={onSubmit}>
            <div className="form__box">
                <label className="form__label">
                    Имя*
                </label>
                <input
                    placeholder="Имя"
                    type="text"
                    value={inputs.firstName}
                    className="form__input"
                    name="firstName"
                    onChange={handleChange}
                />
                { errors.firstName && <span className="form__error">{ errors.firstName }</span> }
            </div>

            <div className="form__box">
                <label className="form__label">
                    Фамилия*
                </label>
                <input
                    placeholder="Фамилия"
                    type="text"
                    value={inputs.lastName}
                    className="form__input"
                    name="lastName"
                    onChange={handleChange}
                />
                { errors.lastName && <span className="form__error">{ errors.lastName }</span> }
            </div>

            <div className="form__box">
                <label className="form__label">
                    Отчество
                </label>
                <input
                    placeholder="Отчество"
                    type="text"
                    value={inputs.middleName}
                    className="form__input"
                    name="middleName"
                    onChange={handleChange}
                />
                { errors.middleName && <span className="form__error">{ errors.middleName }</span> }
            </div>

            <div className="form__box">
                <label className="form__label">
                    Организация
                </label>
                <select
                    placeholder="Организация"
                    className="form__input"
                    name="organisationId"
                    value={inputs.organisationId}
                    onChange={handleChange}
                    disabled={!isEdit}
                >
                    { organisations.map(org => <option key={org.id} value={org.id || ''}>{ org.fullName }</option>) }
                </select>
                { errors.organisationId && <span className="form__error">{ errors.organisationId }</span> }
            </div>

            <div className="form__box">
                <label className="form__label">
                    Email*
                </label>
                <input
                    placeholder="Email"
                    type="email"
                    value={inputs.email}
                    className="form__input"
                    name="email"
                    onChange={handleChange}
                />
                { errors.email && <span className="form__error">{ errors.email }</span> }
            </div>

        </div>
    )
}

export default UserForm