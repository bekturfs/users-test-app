import React from 'react'
import EDIT from '../../assets/icons/edit.svg'
import DELETE from '../../assets/icons/delete.svg'
import Button from "../ui/button"
import clsx from "clsx"

function UserRow({ user, onEditClick, onDeleteClick, isSelected, toggleSelectUser, organisations }) {

    const handleEditClick = e => {
        e.stopPropagation()
        onEditClick(user)
    }

    const handleDeleteClick = e => {
        e.stopPropagation()
        onDeleteClick(user)
    }

    const organisationShortName = organisations.find(o => o.id === user.organisationId)?.shortName || ''

    return (
        <tr className={clsx("users__table_row", isSelected ? 'users__table_row-active' : null)} onClick={() => toggleSelectUser(user)}>
            <td className="users__table_col">
                <input
                    type="checkbox"
                    checked={isSelected}
                    onChange={(e) => toggleSelectUser(user)}
                />
            </td>
            <td className="users__table_col"> { `${user.lastName} ${user.firstName} ${user.middleName}` } </td>
            <td className="users__table_col"> { organisationShortName } </td>
            <td className="users__table_col"> { user.email } </td>
            <td className="users__table_col users__btns">
                <Button className="users__btn" onClick={handleEditClick}>
                    <img src={EDIT}/>
                </Button>
                <Button className="users__btn" onClick={handleDeleteClick}>
                    <img src={DELETE}/>
                </Button>
            </td>
        </tr>
    )
}

export default UserRow