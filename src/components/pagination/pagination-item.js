import React from 'react'
import clsx from 'clsx'

function PaginationItem({ pageNumber, activePage, setActivePage }) {

    const activeClass = pageNumber === activePage ? 'pagination__item_active' : null

    return (
        <li className={clsx("pagination__item", activeClass)}>
            <button
                className="pagination__button"
                onClick={() => setActivePage(pageNumber)}
            >
                { pageNumber }
            </button>
        </li>
    )
}

export default PaginationItem