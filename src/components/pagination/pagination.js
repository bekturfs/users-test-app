import React from 'react'
import '../../styles/pagination.scss'
import PaginationList from "./pagination-list"
import PaginationItem from "./pagination-item"
import Next from '../../assets/icons/next.svg'
import Prev from '../../assets/icons/prev.svg'

function Pagination({ pageNumbers, activePage, setActivePage }) {

    const onNext = () => setActivePage(activePage => activePage < pageNumbers.length ? activePage + 1 : activePage)

    const onPrev = () => setActivePage(activePage => activePage > 1 ? activePage -1 : activePage)

    let pageNumbersToShow = getFiveNeighbours(activePage, pageNumbers)

    return (
        <div className="pagination">
            <PaginationList>
                <button className="pagination__prev-button" onClick={onPrev}>
                    <img src={Prev}/>
                </button>
                {
                    pageNumbersToShow.map(pageNumber =>
                        <PaginationItem
                            key={pageNumber}
                            pageNumber={pageNumber}
                            activePage={activePage}
                            setActivePage={setActivePage}
                        /> )
                }
                <button className="pagination__next-button" onClick={onNext}>
                    <img src={Next}/>
                </button>
            </PaginationList>
        </div>
    )
}
const getFiveNeighbours = (activePage, pages) => {

    const lastPage = pages.length

    if (activePage === 1 || activePage === 2 || activePage === 3) {
        return pages.slice(0, 5)
    } else if (activePage === lastPage - 2 || activePage === lastPage - 1 || activePage === lastPage) {
        return pages.slice(-5)
    } else {
        let pgs = []

        for (let i = activePage - 2; i <= activePage + 2; i++) {
            pgs.push(i)
        }

        return pgs
    }
}

export default Pagination