import React from 'react'

function PaginationList({ children }) {
    return (
        <ul className="pagination__list">
            { children }
        </ul>
    )
}

export default PaginationList