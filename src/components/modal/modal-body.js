import React from 'react'

function ModalBody({ children }) {
    return (
        <div className="modal__body">
            { children }
        </div>
    )
}

export default ModalBody