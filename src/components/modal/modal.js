import React, {useRef} from 'react'
import clsx from "clsx"
import useOnClickOutside from "../../hooks/useOnClickOutside"
import Button from "../ui/button"

function Modal({ children, open = false, onClose = () => {}, size = 'sm' }) {

    const modalContentRef = useRef(null)

    useOnClickOutside(modalContentRef, onClose)

    const activeClass = open ? 'modal-active' : null

    return (
        <div className={clsx("modal", activeClass)}>
            <div ref={modalContentRef} className={clsx("modal__content", `modal__content_${size}`)}>
                { children }
                <Button className="modal__close-btn" onClick={onClose}>
                    X
                </Button>
            </div>
        </div>
    )
}

export default Modal
