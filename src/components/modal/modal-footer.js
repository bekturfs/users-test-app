import React from 'react'

function ModalFooter({ children }) {
    return (
        <div className="modal__footer">
            { children }
        </div>
    )
}

export default ModalFooter