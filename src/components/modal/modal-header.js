import React from 'react'
import Button from "../ui/button"

function ModalHeader({ children }) {
    return (
        <div className="modal__header">
            { children }
        </div>
    )
}

export default ModalHeader