import React from 'react'
import clsx from "clsx"

function Button({ children, color, className, ...rest }) {
    return (
        <button
            className={clsx("btn", color ? `btn__color_${color}` : null, className)}
            { ...rest }
        >
            <span className="btn__inner">
                { children }
            </span>
        </button>
    )
}

export default Button