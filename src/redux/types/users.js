export const SET_USERS = 'SET_USERS'
export const DELETE_USER = 'DELETE_USER'
export const UPDATE_USER = 'UPDATE_USER'
export const DELETE_USERS = 'DELETE_USERS'
export const ADD_USER = 'ADD_USER'