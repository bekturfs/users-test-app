import {ADD_USER, DELETE_USER, DELETE_USERS, SET_USERS, UPDATE_USER} from "../types/users"

export const setUsers = users => ({
    type: SET_USERS,
    payload: { users }
})

export const addUser = newUser => ({
    type: ADD_USER,
    payload: { newUser }
})

export const updateUser = user => ({
    type: UPDATE_USER,
    payload: { user }
})

export const deleteUser = userId => ({
    type: DELETE_USER,
    payload: { userId }
})

export const deleteUsers = usersIds => ({
    type: DELETE_USERS,
    payload: { usersIds }
})