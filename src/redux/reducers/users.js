import { SET_USERS, DELETE_USERS, DELETE_USER, UPDATE_USER, ADD_USER } from "../types/users"

const initialState = {
    users: []
}

export default function usersReducer(state = initialState, action) {
    switch (action.type){
        case SET_USERS:
            return { ...state, users: action.payload.users }
        case ADD_USER:
            return { ...state, users: [...state.users, action.payload.newUser] }
        case UPDATE_USER:
            const userIndex = state.users.findIndex(user => user.id === action.payload.user.id)
            const before = state.users.filter((_, index) => index < userIndex)
            const after = state.users.filter((_, index) => index > userIndex)
            return { ...state, users: [...before, action.payload.user, ...after] }
        case DELETE_USER:
            const newUsers = state.users.filter(user => user.id !== action.payload.userId)
            return { ...state, users: newUsers }
        case DELETE_USERS:
            const users = state.users.filter(user => !action.payload.usersIds.includes(user.id))
            return { ...state, users }
        default:
            return { ...state }
    }
}