export const getUsersWithPagination = (page, itemsPerPage) => {
    return state => {
        const indexOfLast = page * itemsPerPage
        const indexOfFirst = indexOfLast - itemsPerPage

        return state.users.users.slice(indexOfFirst, indexOfLast)
    }
}

export const getPageNumbers = (itemsPerPage) => {
    return state => {

        if (!state.users.users.length) {
            return []
        }

        const pagesCount = Math.ceil(state.users.users.length / itemsPerPage)

        let pageNumbers = []

        for (let i = 1; i <= pagesCount; i++){
            pageNumbers.push(i)
        }

        return pageNumbers
    }
}

export const getMaxId = (state) => {
    try {
        let max = null

        state.users.users.forEach(user => {
            if (user.id > max) {
                max = user.id
            }
        })

        return max
    } catch (e) {
        return null
    }
}