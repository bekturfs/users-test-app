import Users from "./components/users"
import usersData from './data/users.json'
import {useEffect} from "react"
import {useDispatch} from "react-redux"
import {setUsers} from "./redux/actions/users"

import './styles/global.scss'
import './styles/button.scss'
import './styles/form.scss'
import './styles/pagination.scss'
import './styles/modal.scss'
import './styles/users.scss'

function App() {

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setUsers(usersData.users))
    }, [])

  return (
    <div className="App">
      <Users/>
    </div>
  )
}

export default App
